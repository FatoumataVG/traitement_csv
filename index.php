<?php


// En début de script je stock la date
$time_start = microtime(true); 


// Ici tout votre script à mesurer


require 'connexion.php';



/* CHECKING EMAIL */

function verif_mail($email){
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return $email;
    } else {
        return NULL;
    }
}

/* CHANGING PROFESSION Politien TO Menteur */

function menteur($profession){
    if ($profession === "politicien"){

        return str_replace($profession, "menteur", $profession);
    } else {
        return $profession;
    }
}

/* CHANGING PHONE NUMBER TO FRENCH PHONE NUMBER  */

function telFr($phone){
    $zero = "0";
    if (strlen($phone) > 9){
        return $zero . substr($phone, -9);
        
    } else {
        return $zero . $phone;
    }    
}

/* CHANGING CODE COUNTRY BY COUNTRY NAME */

/* CREATE ARRAY FROM countries.csv */

$countries = [];
    
$infos = fopen("countries.csv", "r");
    
while(($data = fgetcsv($infos)) !== false){
    if($data[0] == "name"){
        continue;
    }
    
    $country_fullname = explode(",", $data[0]);
    $countries[$data[2]] = $country_fullname[0];
}
fclose($infos);



function change_country($countries, $country){

   if (isset($countries[$country])) {
        return $countries[$country];
   }
    
}


/* SET BIRTHDATE TO GOOD FORMAT */


function change_format($date) {
    $birthdate = str_replace('/', '-', $date);
    return date('Y-m-d', strtotime($birthdate));
}


    /* ITERATION */


//ouvrir le fichier csv en lecture seule

$infos = fopen("people.csv", "r");



//lire les lignes du fichier une à une
//--------------Tableau où je pousse l'objet tableau----------
$list = array();
//------------------------------------------------------------
$count = 0;
while(($data = fgetcsv($infos)) !== false){

    if($data[0] == "id"){
        continue ;
    }

    $id = $data[0];
    $firstname = $data[1];
    $lastname = $data[2];
    $email = strtolower($data[3]);
    $profession = $data[4];
    $birthdate = $data[5];
    $country = $data[6];
    $phone = $data[7];
    

    $email = verif_mail($email);
    $profession = menteur($profession);
    $country = change_country($countries, $country);
    $birthdate = change_format($birthdate);
    $phone = telFr($phone);

    //pousser toutes les informations de chaque personne dans des tableaux différents.

    array_push($list, array(
        "id" => $id,
        "first_name" => $firstname,
        "last_name" => $lastname,
        "email" => $email,
        "profession" => $profession,
        "birthdate" => $birthdate,
        "country" => $country,
        "phone" => $phone
    ));
    




    //------------------Décommenter pour faire une requête sql-------------
    
    // $mysqli->query("INSERT INTO people (id, firstname, lastname, email, profession, birthdate, country, phone)  VALUE ('$id', '$firstname', '$lastname', '$email', '$profession', '$birthdate', '$country', '$phone')");

}

fclose($infos);

//---------------Décommenter pour créer le fichier avec les données modifiées--------------------


// $fh = fopen("file_created.csv", "w");
// $header = array("id", "firstname", "lastname", "email", "profession","birthdate", "country", "phone");

// fputcsv($fh ,$header);

// foreach($list as $array){
//     fputcsv($fh ,$array);
    
// }
// fclose($fh);






// A la fin du script je récupére la date
$time_end = microtime(true);

// Je calcule la différence (en seconde)
$execution_time = ($time_end - $time_start);

// J'affiche le résultat
echo "Temps d'execution: ".$execution_time."sec";

?>